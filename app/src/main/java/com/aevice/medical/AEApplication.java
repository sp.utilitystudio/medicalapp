package com.aevice.medical;

import android.app.AlarmManager;
import android.app.Application;

import com.aevice.medical.database.PrimaryKeyFactory;
import com.aevice.medical.utils.AlarmUtils;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AEApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("aevice.realm").build();
        Realm.setDefaultConfiguration(config);
        PrimaryKeyFactory.getInstance().initialize(Realm.getDefaultInstance());
        AlarmUtils.getInstance().init(this);
    }
}
