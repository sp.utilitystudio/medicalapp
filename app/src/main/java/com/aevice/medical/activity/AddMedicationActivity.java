package com.aevice.medical.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aevice.medical.R;
import com.aevice.medical.adapter.MedicationAdapter;
import com.aevice.medical.database.Medication;
import com.aevice.medical.databinding.ActivityAddMedicationBinding;
import com.aevice.medical.fragment.AddMedicationFragment;
import com.aevice.medical.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;

public class AddMedicationActivity extends AppCompatActivity implements MedicationAdapter.MedicalAdapterListener {

    ActivityAddMedicationBinding binding;
    DatePickerDialog picker;
    MedicationAdapter medicationAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_medication);
        binding.butCalendar.setOnClickListener(view -> showDatePicker());
        binding.fltAdd.setOnClickListener(view -> startCamera());
        binding.listMedications.setLayoutManager(new LinearLayoutManager(this));
        binding.listMedications.setItemAnimator(new DefaultItemAnimator());

        medicationAdapter = new MedicationAdapter(new ArrayList<>(), this);
        binding.listMedications.setAdapter(medicationAdapter);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();
                if (manager != null) {
                    Fragment fragment = manager.findFragmentById(R.id.frameContent);
                    if (fragment == null) {
                        binding.frameContent.setVisibility(View.GONE);
                        binding.listMedications.setVisibility(View.VISIBLE);
                        binding.fltAdd.show();
                    }
                }
            }
        });

        setToolbar();
        initData();
    }

    private void startCamera() {
        Intent intent = new Intent(this, OCRActivity.class);
        startActivityForResult(intent, 30);
    }

    private void initData() {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Medication> medications = realm.where(Medication.class).findAll();
                runOnUiThread(() -> medicationAdapter.addAll(medications));
            }
        });
    }

    private void addFragment(Medication medication, String ocrResult) {
        binding.frameContent.setVisibility(View.VISIBLE);
        binding.listMedications.setVisibility(View.GONE);

        AddMedicationFragment fragment = new AddMedicationFragment(medication, ocrResult);
        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction1.replace(R.id.frameContent, fragment);
        fragmentTransaction1.addToBackStack(null);
        fragmentTransaction1.commit();
        if (medication != null) {
            binding.edtCalendar.setText(medication.getDate());
        }
        binding.fltAdd.hide();
    }

    public void setToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (binding.frameContent.getVisibility() != View.GONE) {
                    showConfirm();
                } else {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.frameContent.getVisibility() != View.GONE) {
            showConfirm();
        } else {
            super.onBackPressed();
        }
    }

    public void showConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle("Alert");
        builder.setMessage("Are you want sure to discard setting?");
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            super.onBackPressed();
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        });
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    Date tempDate;

    private void showDatePicker() {
        final Calendar cldr = Calendar.getInstance();
        if (tempDate != null) {
            cldr.setTime(tempDate);
        }

        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        picker = new DatePickerDialog(AddMedicationActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        tempDate = calendar.getTime();
                        updateDate(tempDate);
                    }
                }, year, month, day);
        picker.show();
    }

    public void updateDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        binding.edtCalendar.setText(Utils.dateToString(date));
    }

    public void updateData(Medication medication, boolean isUpdate) {
        binding.frameContent.setVisibility(View.GONE);
        binding.listMedications.setVisibility(View.VISIBLE);
        if (isUpdate) {
            medicationAdapter.updateMedication(medication, positionTemp);
        } else {
            medicationAdapter.addMedication(medication);
        }
        binding.fltAdd.show();
    }

    public String getTempDate() {
        return binding.edtCalendar.getText().toString();
    }

    @Override
    public void onItemDeleted(int position) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Medication medication = medicationAdapter.getItem(position);
                RealmResults<Medication> rows = realm.where(Medication.class).equalTo("id", medication.getId()).findAll();
                rows.deleteAllFromRealm();
                medicationAdapter.removeMedication(position);
            }
        });
    }

    int positionTemp = -1;

    @Override
    public void onItemClicked(int position) {
        positionTemp = position;
        Medication medication = medicationAdapter.getItem(position);
        addFragment(medication, "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 30) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("ocr_result");
                addFragment(null, result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                addFragment(null, "");
            }
        }
    }

}
