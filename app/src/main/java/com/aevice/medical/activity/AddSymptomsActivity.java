package com.aevice.medical.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.aevice.medical.database.PrimaryKeyFactory;
import com.aevice.medical.R;
import com.aevice.medical.database.Symptom;
import com.aevice.medical.utils.Utils;
import com.aevice.medical.databinding.ActivityAddSymptomsBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;

public class AddSymptomsActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityAddSymptomsBinding binding;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_symptoms);
        setToolbar();
        initUI();
    }

    public void setToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void initUI() {
        binding.butAdd.setOnClickListener(this);
        binding.butSave.setOnClickListener(this);
        date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
        String formatter = dateFormat.format(date);
        binding.txtDate.setText(formatter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.butAdd:
                binding.editOther.setVisibility(View.VISIBLE);
                break;
            case R.id.butSave:
                saveSymptom();
                break;
        }
    }

    private void saveSymptom() {
        int coughLevel = getIndexRadioChecked(binding.rdgCough);
        int wheezeLevel = getIndexRadioChecked(binding.rdgWheeze);
        String otherSymp = binding.editOther.getText().toString();
        if (coughLevel >= 0 || wheezeLevel >= 0 || TextUtils.isEmpty(otherSymp) == false) {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            Symptom symptom = realm.createObject(Symptom.class,
                    PrimaryKeyFactory.getInstance().nextKey(Symptom.class));
            symptom.setCoughLevel(coughLevel);
            symptom.setWheezeLevel(wheezeLevel);
            symptom.setOtherSymptoms(otherSymp);
            symptom.setDate(Utils.dateToString(date));
            symptom.setTime(Utils.dateToStringTime(date));
            realm.commitTransaction();
            finish();
        }else{
            Toast.makeText(this, "Please fill at least one field", Toast.LENGTH_SHORT).show();
        }
    }

    public int getIndexRadioChecked(RadioGroup radioGroup) {
        int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        if (checkedRadioButtonId == -1) {
            return -1;
        } else {
            int index = radioGroup.indexOfChild(findViewById(checkedRadioButtonId));
            return index;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
