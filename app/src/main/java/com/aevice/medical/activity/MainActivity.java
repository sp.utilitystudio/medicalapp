package com.aevice.medical.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.aevice.medical.R;
import com.aevice.medical.adapter.MedicalTakenAdapter;
import com.aevice.medical.database.Medication;
import com.aevice.medical.databinding.ActivityMainBinding;
import com.aevice.medical.fragment.ConfirmFragment;
import com.aevice.medical.utils.LogUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements MedicalTakenAdapter.MedicalAdapterListener {
    ActivityMainBinding binding;
    MedicalTakenAdapter medicationAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.rlt1.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, AddMedicationActivity.class)));
        binding.rlt2.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, AddSymptomsActivity.class)));
        binding.rlt3.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SummaryActivity.class)));
        initData();

    }

    private void initData() {
        medicationAdapter = new MedicalTakenAdapter(new ArrayList<>(), this);
        binding.rcyMedication.setAdapter(medicationAdapter);
        binding.rcyMedication.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    private void loadData(){
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Medication> medications = realm
                        .where(Medication.class)
                        .findAll();
                List<Medication> medicationList = new ArrayList<>();
                realm.copyFromRealm(medications).stream().forEach(item -> {
                    if(item.getTakenTimes() != null && item.getTakenTimes().size() == item.getIsTakens().size()){
                        //Do nothing
                    }else{
                        medicationList.add(item);
                        LogUtils.d("data: " + new Gson().toJson(item));
                    }
                });
                runOnUiThread(() -> {
                    if(medicationList.size() >0){
                        binding.txtStatus.setText("Have you taken your medication?");
                    }else{
                        binding.txtStatus.setText("You have not added any medication");
                    }
                    medicationAdapter.addAll(medicationList);
                });
            }
        });
    }


    @Override
    public void onItemYes(int position) {
        ConfirmFragment confirmFragment = new ConfirmFragment(medicationAdapter.getItem(position));
        confirmFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                loadData();
            }
        });
        confirmFragment.show(getSupportFragmentManager(), ConfirmFragment.class.getSimpleName());
    }
}
