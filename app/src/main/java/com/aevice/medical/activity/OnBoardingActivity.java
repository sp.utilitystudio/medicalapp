package com.aevice.medical.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.aevice.medical.model.Card;
import com.aevice.medical.adapter.CardAdapter;
import com.aevice.medical.utils.LogUtils;
import com.aevice.medical.R;
import com.aevice.medical.databinding.ActivityOnboardingBinding;
import java.util.ArrayList;

public class OnBoardingActivity extends AppCompatActivity {
    ActivityOnboardingBinding binding;
    int selectedPage = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_onboarding);
        ArrayList<Card> movieList = new ArrayList<>();
        movieList.add(new Card("A"));
        movieList.add(new Card("B"));
        movieList.add(new Card("C"));
        binding.viewPager.setAdapter(new CardAdapter(movieList));
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                LogUtils.d("onPageSelected : " + position);
                selectedPage = position;
                updateLayout();
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateLayout();

    }

    public void updateLayout() {
        String text = "";
        switch (selectedPage){
            case 0:
                text = getString(R.string.onboarding_title_1);
                binding.txtTitle.setText(text);
                binding.txtDescription.setText(text + " - " + getString(R.string.onboarding_des));
                binding.butAction.setText(getString(R.string.onboarding_action_skip));
                binding.butAction.setOnClickListener(view -> handleNavigation());
                break;
            case 1:
                text = getString(R.string.onboarding_title_2);
                binding.txtTitle.setText(text);
                binding.txtDescription.setText(text + " - " + getString(R.string.onboarding_des));
                binding.butAction.setText(getString(R.string.onboarding_action_skip));
                binding.butAction.setOnClickListener(view -> handleNavigation());
                break;
            case 2:
                text = getString(R.string.onboarding_title_3);
                binding.txtTitle.setText(text);
                binding.txtDescription.setText(text + " - " + getString(R.string.onboarding_des));
                binding.butAction.setText(getString(R.string.onboarding_action_get_started));
                binding.butAction.setOnClickListener(view -> handleNavigation());
                break;
        }

    }

    public void handleNavigation(){
        startActivity(new Intent(OnBoardingActivity.this, MainActivity.class));
        finish();
    }

}
