package com.aevice.medical.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.MenuItem;

import com.aevice.medical.utils.LogUtils;
import com.aevice.medical.R;
import com.aevice.medical.model.SummaryModel;
import com.aevice.medical.adapter.SampleAdapter;
import com.aevice.medical.database.Medication;
import com.aevice.medical.database.Symptom;
import com.aevice.medical.databinding.ActivitySummaryBinding;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import io.realm.Realm;
import io.realm.RealmResults;

public class SummaryActivity extends AppCompatActivity {

    ActivitySummaryBinding binding;
    SampleAdapter sampleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_summary);
        setToolbar();
        initData();
    }

    private void initData() {


        RealmResults<Medication> medications = Realm.getDefaultInstance().where(Medication.class).findAll();
        RealmResults<Symptom> symptoms = Realm.getDefaultInstance().where(Symptom.class).findAll();

        List<SummaryModel> summaryModelList = new ArrayList<>();

        Realm.getDefaultInstance().copyFromRealm(medications).stream().forEach(medication -> {
            LogUtils.d("Medication : " + new Gson().toJson(medication));
            if(medication.getReminderTimes().size() > 0){
                for(int i= 0; i< medication.getReminderTimes().size(); i++){
                    String remindTime = medication.getReminderTimes().get(i);
                    SummaryModel summaryModel = new SummaryModel();
                    summaryModel.setDate(medication.getDate());
                    if(medication.getIsTakens().get(i)){
                        summaryModel.setTime(medication.getTakenTimes().get(i));
                        summaryModel.setContent("[Done]" + medication.getDoses() + " tablet of " + medication.getName() + " taken");
                    }else{
                        summaryModel.setTime(remindTime);
                        summaryModel.setContent("[Waiting] take " +medication.getDoses() + " tablet of " + medication.getName());
                    }

                    summaryModelList.add(summaryModel);
                }
            }
        });

        Realm.getDefaultInstance().copyFromRealm(symptoms).stream().forEach(symptom -> {
            SummaryModel summaryModel = new SummaryModel();
            summaryModel.setDate(symptom.getDate());
            summaryModel.setTime(symptom.getTime());
            summaryModel.setContent(symptom.getOtherSymptoms());
            summaryModelList.add(summaryModel);
        });

        sampleAdapter = new SampleAdapter(this, summaryModelList);
        binding.rcySummary.setAdapter(sampleAdapter);

    }

    public void setToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
