package com.aevice.medical.adapter;

import android.view.View;
import android.widget.TextView;

import com.aevice.medical.model.Card;
import com.aevice.medical.R;
import com.github.islamkhsh.CardSliderAdapter;
import java.util.ArrayList;

public class CardAdapter extends CardSliderAdapter<Card> {

    ArrayList<Card> data = new ArrayList<>();
    public CardAdapter(ArrayList<Card> items) {
        super(items);
        data = items;
    }

    @Override
    public void bindView(int position, View itemContentView, Card item) {
        Card card = data.get(position);
        TextView textView =  itemContentView.findViewById(R.id.txtTitle);
        textView.setText(card.getTitle());
    }

    @Override
    public int getItemContentLayout(int position) {
        return R.layout.item_intro;
    }
}
