package com.aevice.medical.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aevice.medical.R;
import com.aevice.medical.database.Medication;
import com.aevice.medical.databinding.ItemMedicationTakenBinding;

import java.util.List;

public class MedicalTakenAdapter extends RecyclerView.Adapter<MedicalTakenAdapter.MedicationTakenViewHolder> {

    private List<Medication> medicationList;
    private MedicalTakenAdapter.MedicalAdapterListener listener;

    public MedicalTakenAdapter(List<Medication> timeRemindList, MedicalTakenAdapter.MedicalAdapterListener listener) {
        this.medicationList = timeRemindList;
        this.listener = listener;
    }

    public boolean addMedication(Medication medication) {
        medicationList.add(medication);
        notifyDataSetChanged();
        return true;
    }

    public void addAll(List<Medication> medications) {
        medicationList.clear();
        medicationList.addAll(medications);
        notifyDataSetChanged();
    }

    public List<Medication> getData() {
        return medicationList;
    }

    public void updateMedication(Medication medication, int position) {
        medicationList.set(position, medication);
        notifyItemChanged(position);
    }

    public boolean removeMedication(int position) {
        medicationList.remove(position);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public MedicationTakenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMedicationTakenBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_medication_taken, parent, false);
        return new MedicationTakenViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MedicationTakenViewHolder holder, final int position) {
        Medication medication = medicationList.get(position);
        holder.binding.txtMedical.setText(medication.getName());
        holder.binding.txtDose.setText(medication.getDoses() + " tablet");
        holder.binding.txtFreq.setText(medication.getFreqs() + " times a day");
        String meal = medication.isBeforeMeal() ? "Before Meal" : "After Meal";
        holder.binding.txtMeal.setText(meal);
        holder.binding.butYes.setOnClickListener(view -> listener.onItemYes(position));
    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }

    public Medication getItem(int position) {
        return medicationList.get(position);
    }

    public interface MedicalAdapterListener {
        void onItemYes(int position);
    }

    public class MedicationTakenViewHolder extends RecyclerView.ViewHolder {

        private final ItemMedicationTakenBinding binding;

        public MedicationTakenViewHolder(final ItemMedicationTakenBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

}
