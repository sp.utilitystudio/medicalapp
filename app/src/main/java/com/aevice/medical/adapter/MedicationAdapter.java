package com.aevice.medical.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aevice.medical.R;
import com.aevice.medical.database.Medication;
import com.aevice.medical.databinding.ItemMedicationBinding;

import java.util.List;

public class MedicationAdapter extends RecyclerView.Adapter<MedicationAdapter.MedicationListViewHolder> {

    private List<Medication> medicationList;
    private MedicalAdapterListener listener;

    public MedicationAdapter(List<Medication> timeRemindList, MedicalAdapterListener listener) {
        this.medicationList = timeRemindList;
        this.listener = listener;
    }

    public boolean addMedication(Medication medication) {
        medicationList.add(medication);
        notifyDataSetChanged();
        return true;
    }

    public void addAll(List<Medication> medications) {
        medicationList.clear();
        medicationList.addAll(medications);
        notifyDataSetChanged();
    }

    public List<Medication> getData() {
        return medicationList;
    }

    public void updateMedication(Medication medication, int position) {
        medicationList.set(position, medication);
        notifyItemChanged(position);
    }

    public boolean removeMedication(int position) {
        medicationList.remove(position);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public MedicationListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMedicationBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_medication, parent, false);
        return new MedicationListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MedicationListViewHolder holder, final int position) {
        Medication medication = medicationList.get(position);
        holder.binding.txtName.setText(medication.getName());
        holder.binding.txtDate.setText(medication.getDate());
        holder.binding.butDelete.setOnClickListener(view -> listener.onItemDeleted(position));
        //TODO: Remove Edit feature
        //holder.binding.getRoot().setOnClickListener(view -> listener.onItemClicked(position));
    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }

    public Medication getItem(int position) {
        return medicationList.get(position);
    }

    public interface MedicalAdapterListener {
        void onItemDeleted(int position);
        void onItemClicked(int position);
    }

    public class MedicationListViewHolder extends RecyclerView.ViewHolder {

        private final ItemMedicationBinding binding;

        public MedicationListViewHolder(final ItemMedicationBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

}
