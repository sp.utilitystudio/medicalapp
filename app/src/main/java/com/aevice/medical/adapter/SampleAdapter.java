package com.aevice.medical.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aevice.medical.utils.LogUtils;
import com.aevice.medical.R;
import com.aevice.medical.model.SummaryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SampleAdapter extends StickyHeaderGridAdapter {
    private Context context;
    private List<SummaryModel> summaryModels = new ArrayList();
    private List<String> dateList = new ArrayList();

    public SampleAdapter(Context context, List<SummaryModel> summaryModels) {
        this.context = context;
        this.summaryModels = summaryModels;
        initData();
    }

    private void initData() {
        Set<String> dateSet = new HashSet<>();
        if (summaryModels.size() > 0) {
            summaryModels.stream().forEach(summary -> {
                dateSet.add(summary.getDate());
                LogUtils.d("Date set : " + summary.getDate());
            });
        }
        dateList.addAll(dateSet);
        Collections.sort(dateList, new StringDateComparator());
    }

    public int getSectionCount() {
        return dateList.size();
    }

    public int getSectionItemCount(int section) {
        String date = dateList.get(section);
        return summaryModels.stream().filter(summaryModel -> summaryModel.getDate().equals(date))
                .collect(Collectors.toList()).size();
    }


    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        return new MyHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_header, parent, false));
    }

    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        return new MyItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_item, parent, false));
    }

    public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int section) {
        ((MyHeaderViewHolder) viewHolder).labelView.setText(this.dateList.get(section));
    }

    public void onBindItemViewHolder(ItemViewHolder viewHolder, int section, final int position) {
        final MyItemViewHolder holder = (MyItemViewHolder) viewHolder;
        String date = dateList.get(section);
        SummaryModel model = summaryModels.stream().filter(summaryModel -> summaryModel.getDate().equals(date))
                .collect(Collectors.toList()).get(position);
        holder.labelView.setText(model.getContent());
        int hour = Integer.parseInt(model.getTime().substring(0, 2));
        int minute = Integer.parseInt(model.getTime().substring(3));
        holder.txtTime.setText(String.format("%02d", hour)
                + ":" + String.format("%02d", minute));
    }

    public static class MyHeaderViewHolder extends HeaderViewHolder {
        TextView labelView;

        MyHeaderViewHolder(View itemView) {
            super(itemView);
            this.labelView = (TextView) itemView.findViewById(R.id.label);
        }
    }

    public static class MyItemViewHolder extends ItemViewHolder {
        TextView labelView;
        TextView txtTime;

        MyItemViewHolder(View itemView) {
            super(itemView);
            this.txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            this.labelView = (TextView) itemView.findViewById(R.id.txtLabel);
        }
    }

    class StringDateComparator implements Comparator<String> {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        public int compare(String lhs, String rhs) {
            try {
                return dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs)) * (-1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return -1;
        }
    }


}
