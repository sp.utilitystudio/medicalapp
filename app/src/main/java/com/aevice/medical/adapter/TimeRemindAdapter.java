package com.aevice.medical.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aevice.medical.R;
import com.aevice.medical.databinding.ItemTimeRemindBinding;
import com.aevice.medical.model.TimeRemind;

import java.util.List;

public class TimeRemindAdapter extends RecyclerView.Adapter<TimeRemindAdapter.MyViewHolder> {

    private List<TimeRemind> timeReminds;
    private LayoutInflater layoutInflater;
    private TimesAdapterListener listener;
    private int type = 1;

    public void addAll(List<TimeRemind> remindList) {
        timeReminds.clear();
        timeReminds.addAll(remindList);
        notifyDataSetChanged();
    }


    public TimeRemindAdapter(List<TimeRemind> timeRemindList) {
        this.timeReminds = timeRemindList;
    }

    public void setListener(TimesAdapterListener listener){
        this.listener = listener;

    }

    public boolean addTimeRemind(TimeRemind timeRemind) {
        timeReminds.add(timeRemind);
        notifyDataSetChanged();
        return true;
    }

    public List<TimeRemind> getData() {
        return timeReminds;
    }

    public void updateTimeRemind(TimeRemind timeRemind, int position) {
        timeReminds.set(position, timeRemind);
        notifyItemChanged(position);
    }

    public boolean removeTimeRemind(int position) {
        timeReminds.remove(position);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemTimeRemindBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_time_remind, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        TimeRemind timeRemind = timeReminds.get(position);
        holder.binding.txtTime.setText(String.format("%02d", timeRemind.getHour())
                + ":" + String.format("%02d", timeRemind.getMinute()));
        if (type == 2) {
            holder.binding.butEdit.setVisibility(View.INVISIBLE);
            holder.binding.butDelete.setVisibility(View.INVISIBLE);
        }
        holder.binding.butEdit.setOnClickListener(view -> listener.onItemEdited(position));
        holder.binding.butDelete.setOnClickListener(view -> listener.onItemDeleted(position));
    }

    @Override
    public int getItemCount() {
        return timeReminds.size();
    }

    public TimeRemind getItem(int position) {
        return timeReminds.get(position);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ItemTimeRemindBinding binding;

        public MyViewHolder(final ItemTimeRemindBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public interface TimesAdapterListener {
        void onItemEdited(int position);

        void onItemDeleted(int position);
    }

}
