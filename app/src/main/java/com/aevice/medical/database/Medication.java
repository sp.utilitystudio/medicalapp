package com.aevice.medical.database;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Medication extends RealmObject {
    @PrimaryKey
    private long id;
    private String date;
    private String name;
    private boolean beforeMeal;
    private boolean isTaken;
    private int doses;
    private int freqs;
    private RealmList<String> reminderTimes = new RealmList<>();
    private RealmList<String> takenTimes = new RealmList<>();
    private RealmList<Boolean> isTakens = new RealmList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isBeforeMeal() {
        return beforeMeal;
    }

    public void setBeforeMeal(boolean beforeMeal) {
        this.beforeMeal = beforeMeal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDoses() {
        return doses;
    }

    public void setDoses(int doses) {
        this.doses = doses;
    }

    public int getFreqs() {
        return freqs;
    }

    public void setFreqs(int freqs) {
        this.freqs = freqs;
    }

    public RealmList<String> getReminderTimes() {
        return reminderTimes;
    }

    public void setReminderTimes(RealmList<String> reminderTimes) {
        this.reminderTimes = reminderTimes;
    }

    public RealmList<Boolean> getIsTakens() {
        return isTakens;
    }

    public void setIsTakens(RealmList<Boolean> isTakens) {
        this.isTakens = isTakens;
    }

    public RealmList<String> getTakenTimes() {
        return takenTimes;
    }

    public void setTakenTimes(RealmList<String> takenTimes) {
        this.takenTimes = takenTimes;
    }
}
