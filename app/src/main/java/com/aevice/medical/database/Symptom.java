package com.aevice.medical.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Symptom extends RealmObject {
    @PrimaryKey
    private long id;
    private int coughLevel;
    private int wheezeLevel;
    private String otherSymptoms;
    private String date;
    private String time;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCoughLevel() {
        return coughLevel;
    }

    public void setCoughLevel(int coughLevel) {
        this.coughLevel = coughLevel;
    }

    public int getWheezeLevel() {
        return wheezeLevel;
    }

    public void setWheezeLevel(int wheezeLevel) {
        this.wheezeLevel = wheezeLevel;
    }

    public String getOtherSymptoms() {
        return otherSymptoms;
    }

    public void setOtherSymptoms(String otherSymptoms) {
        this.otherSymptoms = otherSymptoms;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
