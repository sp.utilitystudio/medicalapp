package com.aevice.medical.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aevice.medical.utils.AlarmReceiver;
import com.aevice.medical.utils.AlarmUtils;
import com.aevice.medical.utils.LogUtils;
import com.aevice.medical.R;
import com.aevice.medical.model.TimeRemind;
import com.aevice.medical.adapter.TimeRemindAdapter;
import com.aevice.medical.activity.AddMedicationActivity;
import com.aevice.medical.database.Medication;
import com.aevice.medical.database.PrimaryKeyFactory;
import com.aevice.medical.databinding.FragmentAddMedicationBinding;
import com.aevice.medical.utils.Utils;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Consumer;

import io.realm.Realm;


public class AddMedicationFragment extends Fragment
        implements TimeRemindAdapter.TimesAdapterListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener {

    FragmentAddMedicationBinding binding;
    TimeRemindAdapter timeRemindAdapter = null;
    int numberOfDose = 0;
    int numberOfFreq = 0;
    Medication bundle;
    String ocrResult;

    public AddMedicationFragment(Medication medication, String ocrResult) {
        this.bundle = medication;
        this.ocrResult = ocrResult;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_medication, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(TextUtils.isEmpty(ocrResult) == false){
            binding.editDrug.setText(ocrResult);
        }

        binding.rvReminds.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvReminds.setItemAnimator(new DefaultItemAnimator());

        timeRemindAdapter = new TimeRemindAdapter(new ArrayList<>());
        timeRemindAdapter.setListener(this);
        binding.rvReminds.setAdapter(timeRemindAdapter);

        binding.butAdd.setOnClickListener(view1 -> showTimePicker(this, null));
        binding.doseUp.setOnClickListener(this);
        binding.doseDown.setOnClickListener(this);
        binding.freqUp.setOnClickListener(this);
        binding.freqDown.setOnClickListener(this);
        binding.butSave.setOnClickListener(view12 -> saveMedication());
        initUI();
    }

    private void initUI(){
        if(bundle != null){
            binding.editDrug.setText(bundle.getName());
            numberOfDose = bundle.getDoses();
            numberOfFreq = bundle.getFreqs();
            binding.txtDose.setText(numberOfDose + "");
            binding.txtFreq.setText(numberOfFreq + "");
            binding.butBefore.setChecked(bundle.isBeforeMeal());
            binding.butAfter.setChecked(!bundle.isBeforeMeal());
            List<TimeRemind> remindList = new ArrayList<>();
            bundle.getReminderTimes().stream().forEach(new Consumer<String>() {
                @Override
                public void accept(String remind) {
                    int hour = Integer.parseInt(remind.substring(0, 2));
                    int minute = Integer.parseInt(remind.substring(3));
                    TimeRemind timeRemind = new TimeRemind(hour, minute);
                    remindList.add(timeRemind);
                }
            });
            timeRemindAdapter.addAll(remindList);
        }
    }

    private void saveMedication() {
        if (validateData() == false) return;

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Medication medication = null;
        if(bundle != null){
            medication = realm.copyToRealm(bundle);
        }else {
            medication = realm.createObject(Medication.class, PrimaryKeyFactory.getInstance().nextKey(Medication.class));
        }
        String medicationName = binding.editDrug.getText().toString();
        medication.setName(medicationName);
        medication.setDoses(Integer.parseInt(binding.txtDose.getText().toString()));
        medication.setFreqs(Integer.parseInt(binding.txtFreq.getText().toString()));
        medication.setBeforeMeal(binding.butBefore.isChecked());
        medication.getReminderTimes().clear();
        String date = ((AddMedicationActivity)getActivity()).getTempDate();
        medication.setDate(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(Utils.stringToDate(date));
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        for(TimeRemind timeRemind: timeRemindAdapter.getData()){
            String time = String.format("%02d", timeRemind.getHour())
                    + ":" + String.format("%02d", timeRemind.getMinute());
            medication.getReminderTimes().add(time);
            medication.getIsTakens().add(false);
            AlarmUtils.getInstance().setReminder(getActivity(), AlarmReceiver.class,
                    year, month, day,
                    timeRemind.getHour(), timeRemind.getMinute());
        }
        realm.copyToRealmOrUpdate(medication);
        realm.commitTransaction();

        if(bundle != null){
            ((AddMedicationActivity)getActivity()).updateData(medication, true);
        }else{
            ((AddMedicationActivity)getActivity()).updateData(medication, false);
        }



    }

    private boolean validateData() {
        if(TextUtils.isEmpty(((AddMedicationActivity)getActivity()).getTempDate())){
            Toast.makeText(getActivity(), "Please fill the date of medication", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(TextUtils.isEmpty(binding.editDrug.getText().toString())){
            Toast.makeText(getActivity(), "Please fill the medication label!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numberOfDose == 0){
            Toast.makeText(getActivity(), "Please fill the quantity of dose!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numberOfFreq == 0){
            Toast.makeText(getActivity(), "Please fill frequency of medication!", Toast.LENGTH_SHORT).show();
            return false;
        }

        int size = timeRemindAdapter.getItemCount();
        if(size != numberOfFreq){
            Toast.makeText(getActivity(), "Please fill the remind time to match frequency!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numberOfDose % numberOfFreq != 0){
            Toast.makeText(getActivity(), "Please fill number of dose to match frequency!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    int editPosition = -1;
    public void showTimePicker(TimePickerDialog.OnTimeSetListener callback, TimeRemind data) {
        TimeRemind temp;
        if(data == null){
            temp = new TimeRemind(Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                    Calendar.getInstance().get(Calendar.MINUTE));
        }else{
            temp = data;
        }
        TimePickerDialog tpd = TimePickerDialog.newInstance(callback, temp.getHour(), temp.getMinute(), 0, false);
        tpd.setOnCancelListener(dialogInterface -> editPosition = -1);
        tpd.setVersion(TimePickerDialog.Version.VERSION_1);
        tpd.setAccentColor(ContextCompat.getColor(getActivity(), R.color.mdtp_accent_color_dark));
        tpd.show(getActivity().getSupportFragmentManager(), "time");
    }


    @Override
    public void onItemEdited(int position) {
        editPosition = position;
        showTimePicker(this, timeRemindAdapter.getItem(position));
    }

    @Override
    public void onItemDeleted(int position) {
        timeRemindAdapter.removeTimeRemind(position);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        LogUtils.i("Edit position " + editPosition);
        if(editPosition >= 0){
            timeRemindAdapter.updateTimeRemind(new TimeRemind(hourOfDay, minute), editPosition);
            editPosition = -1;
        }else{
            TimeRemind remind = new TimeRemind(hourOfDay, minute);
            timeRemindAdapter.addTimeRemind(remind);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.doseUp:
                if(numberOfDose < 10) {
                    numberOfDose++;
                    binding.txtDose.setText(numberOfDose + "");
                }
                break;
            case R.id.doseDown:
                if(numberOfDose > 0) {
                    numberOfDose--;
                    binding.txtDose.setText(numberOfDose + "");
                }
                break;
            case R.id.freqUp:
                if(numberOfFreq < 5){
                    numberOfFreq ++;
                    binding.txtFreq.setText(numberOfFreq + "");
                }
                break;
            case R.id.freqDown:
                if(numberOfFreq > 0){
                    numberOfFreq--;
                    binding.txtFreq.setText(numberOfFreq + "");
                }
                break;
        }
    }
}
