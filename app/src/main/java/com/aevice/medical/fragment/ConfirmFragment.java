package com.aevice.medical.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aevice.medical.R;
import com.aevice.medical.adapter.TimeRemindAdapter;
import com.aevice.medical.database.Medication;
import com.aevice.medical.databinding.FragmentConfirmBinding;
import com.aevice.medical.model.TimeRemind;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmFragment extends DialogFragment implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    FragmentConfirmBinding binding;
    Medication medication;
    TimeRemindAdapter timeRemindAdapter = null;
    DialogInterface.OnDismissListener listener;

    public ConfirmFragment(Medication medication) {
        this.medication = medication;
    }

    public static ConfirmFragment newInstance(Medication medication) {
        return new ConfirmFragment(medication);
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener){
        this.listener = listener;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        listener.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String formatter = dateFormat.format(Calendar.getInstance().getTime());
        binding.txtNow.setText(formatter);

        List<TimeRemind> timeReminds = new ArrayList<>();

        for (int i = 0; i < medication.getReminderTimes().size(); i++) {
            String remind = medication.getReminderTimes().get(i);
            boolean isTaken = medication.getIsTakens().get(i);

            int hour = Integer.parseInt(remind.substring(0, 2));
            int minute = Integer.parseInt(remind.substring(3));
            TimeRemind timeRemind = new TimeRemind(hour, minute);
            if (isTaken == true) {
                timeReminds.add(timeRemind);
            }
        }

        if (timeReminds.size() > 0) {
            timeRemindAdapter = new TimeRemindAdapter(timeReminds);
            timeRemindAdapter.setType(2);
            binding.rvReminds.setAdapter(timeRemindAdapter);
            binding.rvReminds.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            binding.txtPrevious.setText("No taken history previously");
        }

        binding.butSave.setOnClickListener(this);
        binding.butUpdate.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.butSave:
                saveTimeTaken();
                break;
            case R.id.butUpdate:
                showTimePicker();
                break;
        }
    }

    private void saveTimeTaken() {
        Realm realm =  Realm.getDefaultInstance();
        realm.beginTransaction();
        Medication result = realm
                .where(Medication.class)
                .equalTo("id", medication.getId())
                .findFirst();
        for (int i = 0; i < result.getIsTakens().size(); i++) {
            boolean isTaken = result.getIsTakens().get(i);
            if(isTaken == false){
                result.getIsTakens().set(i, true);
                result.getTakenTimes().add(binding.txtNow.getText().toString());
                break;
            }
        }
        realm.copyToRealmOrUpdate(result);
        realm.commitTransaction();
        dismiss();
    }

    private void showTimePicker() {
        TimeRemind temp = new TimeRemind(Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE));
        TimePickerDialog tpd = TimePickerDialog.newInstance(this, temp.getHour(), temp.getMinute(), 0, false);
        tpd.setVersion(TimePickerDialog.Version.VERSION_1);
        tpd.setAccentColor(ContextCompat.getColor(getActivity(), R.color.mdtp_accent_color_dark));
        tpd.show(getActivity().getSupportFragmentManager(), "time");
    }


    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        binding.txtNow.setText(String.format("%02d", hourOfDay)
                + ":" + String.format("%02d", minute));
    }
}
