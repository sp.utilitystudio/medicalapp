package com.aevice.medical.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.aevice.medical.activity.MainActivity;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtils.d("AlarmReceiver " + intent);
        Intent startIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        Utils.showNotification(context, "Medical App", "Reminder to take medicine", startIntent);
    }
}
