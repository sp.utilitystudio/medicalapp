package com.aevice.medical.utils;

import android.util.Log;

public class LogUtils {

    public static final String TAG = "LogAE";

    public static void d(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.d(TAG, text + message);
    }

    public static void i(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.i(TAG, text + message);
    }

    public static void e(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.e(TAG, text + message);
    }

}
